#
# Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
# Distributed under the GPL-3
#
# Top-level make file.  This Makefile assumes
#
# - HepMC3 is installed as two version (mine and master) in
#   - ../install-mine
#   - ../install-master
#
# - Pythia8 is installed in /opt/sw/inst
#
# To change this,
#
# - change definitions of HEPMC3_CONFIG in this Makefile and in eg/Makefile
# - change definition of PYTHIA8_CONFIG in eg/Makefile 
#
#
ifneq ($(findstring mine,$(MAKECMDGOALS)),)
HEPMC3_CONFIG	:= ../install_mine/bin/HepMC3-config
else
HEPMC3_CONFIG	:= ../install_master/bin/HepMC3-config
endif
VERSION		:= $(patsubst install_%,%,\
                     $(notdir $(shell $(HEPMC3_CONFIG) --prefix)))
HEPMC3_CPPFLAGS	:= $(shell $(HEPMC3_CONFIG) --cppflags)
HEPMC3_LIBDIR	:= $(shell $(HEPMC3_CONFIG) --libdir)
HEPMC3_LDFLAGS	:= -L$(HEPMC3_LIBDIR) -Wl,-rpath,$(HEPMC3_LIBDIR)
HEPMC3_LIBS	:= $(filter -l%, $(shell $(HEPMC3_CONFIG) --libs --search))

CPPFLAGS	:= $(HEPMC3_CPPFLAGS) -I. -D$(VERSION)=1
CXX		:= g++
CXXFLAGS	:= -g -c
LD		:= g++
LDFLAGS		:= $(HEPMC3_LDFLAGS)
LIBS		:= $(HEPMC3_LIBS)

HEADERS		:= bench/driver.hh	\
		   bench/logger.hh	\
		   bench/options.hh	\
		   bench/reader.hh	\
		   bench/timer.hh	\
		   bench/version.hh

TESTS		:= tests/read.cc		\
		   tests/write.cc		\
		   tests/print.cc 		\
		   tests/add_attr.cc		\
		   tests/add_part.cc		\
		   tests/rem_vtx.cc		\
		   tests/rem_part.cc		\
		   tests/cnt_anc.cc		\
		   tests/cnt_dec.cc		\
		   tests/rotate.cc		\
		   tests/new_delete.cc		\
		   tests/add_tree.cc		

DATA_URL	:= https://rivetval.web.cern.ch/rivetval/HEPMC/
DATA_FILES	:= LHC-13-Minbias		\
		   LEP-93.0			\
		   LHC-13-Top-All		\
		   LHC-7-Dijets-2-C		\
		   BFactory-10.45

INPUT		:= all.hepmc
INPUT		:= inputs/LHC-13-Minbias.hepmc 

BIN_TESTS	:= $(TESTS:tests/%.cc=%_$(VERSION))
LOG_TESTS	:= $(BIN_TESTS:%=%.log)
PERF_TESTS	:= $(BIN_TESTS:%=%.perf)
HTML_TESTS	:= $(BIN_TESTS:%=%.html)
DS_TESTS	:= $(LOG_TESTS:%.log=%_$(DATASET).log)

PERF_EXEC_PATH	:= /usr/lib/perf-core
RECORD		:= $(PERF_EXEC_PATH)/scripts/python/bin/flamegraph-record
REPORT		:= $(PERF_EXEC_PATH)/scripts/python/bin/flamegraph-report
RECORD_OPTIONS	:= -i $(INPUT)


vpath	%.cc tests

%_$(VERSION).o:%.cc $(HEADERS)
	$(CXX) $(CPPFLAGS) $(CXXFLAGS) $< -o $@

%:%.o
	$(LD) $(LDFLAGS) $^ -o $@ $(LIBS)

%.log:%
	./$< -t -i $(INPUT) $(EXTRA) > $@ $(IGNORE_FAILURE)

%.perf:%
	sudo $(RECORD) -o $@ -- ./$< $(RECORD_OPTIONS)

%.html:%.perf
	sudo chown $(USER) $<
	ln -sf $< perf.data
	PERF_EXEC_PATH=$(PERF_EXEC_PATH) $(REPORT) -o $@ --allow-download
	rm perf.data

%_$(VERSION)_$(DATASET).log:%_$(VERSION)
	./$< -t -i inputs/$(DATASET).hepmc $(EXTRA) > $@ $(IGNORE_FAILURE)

%_$(VERSION)_$(DATASET).perf:%_$(VERSION)
	sudo $(RECORD) -o $@ -- ./$< $(RECORD_OPTIONS)

%_$(VERSION)_$(DATASET).perf:	INPUT=inputs/$(DATASET).hepmc

%.hepmc.gz:
	curl $(DATA_URL)$@

all:	$(BIN_TESTS)
logs:	$(LOG_TESTS)
dslogs:	$(DS_TESTS)
perfs:  $(PERF_TESTS)
htmls:  $(HTML_TESTS)

show:
	@echo "HEPMC3_CONFIG    := $(HEPMC3_CONFIG)"
	@echo "HEPMC3_CPPFLAGS  := $(HEPMC3_CPPFLAGS)"
	@echo "HEPMC3_LDFLAGS   := $(HEPMC3_LDFLAGS)"
	@echo "HEPMC3_LIBS      := $(HEPMC3_LIBS)"
	@echo "VERSION          := $(VERSION)"
	@echo "DATA_URL         := $(DATA_URL)"
	@echo "TESTS            := $(foreach t, $(sort $(TESTS)),$(notdir $(basename $(t))),)"

clean:
	rm -f *~ *.o *~ 
	rm -f $(TESTS:tests/%.cc=%_*)
	rm -f bench/*~
	rm -f tests/*~
	rm -f analysis/*~
	rm -f *.gv*
	rm -rf analysis/.ipynb_checkpoints
	rm -rf analysis/__pycache__

realclean: clean
	rm -f *.log *.perf *.html 

mine:
	@echo "Nothing"

master:
	@echo "Nothing"

# Special treatment of code that either fails or take a very long time
# to finish.
rem_vtx_master.log:		IGNORE_FAILURE=|| true
rem_part_master.log:		IGNORE_FAILURE=|| true
rem_vtx_master.log:		EXTRA=-n 100
rem_part_master.log:		EXTRA=-n 100
rem_vtx_master_$(DATASET).log:	IGNORE_FAILURE=|| true
rem_part_master_$(DATASET).log:	IGNORE_FAILURE=|| true
rem_vtx_master_$(DATASET).log:	EXTRA=-n 100
rem_part_master_$(DATASET).log:	EXTRA=-n 100


all.hepmc:
	$(MAKE) -C eg
	cp eg/all.hepmc .

7.hepmc:all.hepmc write_master 
	./write_master -i $< -f 7 -n 7 -v 2> $@ 

59.hepmc:all.hepmc write_master 
	./write_master -i $< -f 60 -n 60 -v 2> $@ 

#
# EOF
#
