# Analysis notebooks 

Each notebook summaries the result of running a test.  The results are
plotted and the complexity of each test is determined. 

**Note** These notebooks (more specifically the file
[`bench.py`](bench.py)) used the package `nbi_stat`.  If you do not
have that package, do 

    pip install nbi_stat 
    
