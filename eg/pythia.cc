//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <Pythia8/Pythia.h>
#ifdef OLD_INTERFACE
#include "Pythia8ToHepMC3.h"
#else
#include <Pythia8Plugins/HepMC3.h>
#endif
#include <HepMC3/WriterAscii.h>

// ===================================================================
struct options {
  std::string outputFileName = "-";
  size_t nev = 1000;
  int seed = 1234;
  int verbose = 0;
};

  
// ===================================================================
void usage(const std::string& progname,
           const options&     options,
           std::ostream&      out=std::cout)
{
  out << "Usage: " << progname << " [OPTIONS]\n\n"
      << "Options:\n"
      << "  -o,--output   OUTPUT  Oupput file name ("
      << options.outputFileName << ")\n"
      << "  -n,--nevents  NEV     Number of events ("
      << options.nev << ")\n"
      << std::endl;
}

// ===================================================================
int parse(int argc, char** argv, options& opt) {
  for (int i = 1; i < argc; ++i) {
    if (argv[i][0] == '-') {
      if (argv[i][1] == '-') {
        std::string arg(argv[i]);
          
        if (arg == "--help") {
          usage(argv[0], opt);
          return 0;
        }
        else if (arg == "--output")
          opt.outputFileName = argv[++i];
        else if (arg == "--nevents")
          opt.nev = std::stoi(argv[++i]);
        else if (arg == "--seed")
          opt.nev = std::stoi(argv[++i]);
        else if (arg == "--verbose")
          opt.verbose++;
        else {
          std::cerr << "Unknown option: " << argv[i] << std::endl;
          return 1;
        }
          
        continue;
      }
      switch (argv[i][1]) {
      case 'h': usage(argv[0],opt);                       return 0;
      case 'o': opt.outputFileName = argv[++i];           break;
      case 'n': opt.nev            = std::stoi(argv[++i]); break;
      case 's': opt.seed           = std::stoi(argv[++i]); break;
      case 'v': opt.verbose++;                            break;
      default:
        std::cerr << "Unknwon opton: " << argv[i] << std::endl;
        return 1;
      }
    }
    else
      opt.outputFileName = argv[i];
    }
    return 1;
  }

// ===================================================================
int main(int argc, char** argv)
{
  options opts;
  int ret = parse(argc, argv, opts);
  if (ret <= 0) return -ret;

  Pythia8::Pythia pythia("",false);

  // Silence pythia 
  pythia.readString("Print:quiet                      = on");
  pythia.readString("Init:showAllSettings             = off");
  pythia.readString("Stat:showProcessLevel            = off");
  pythia.readString("Init:showAllParticleData         = off");
  pythia.readString("Init:showChangedParticleData     = off");
  pythia.readString("Init:showChangedSettings         = off");
  pythia.readString("Init:showMultipartonInteractions = off");
  
  // Set seed
  pythia.readString("Random:setSeed = "
                    +std::string(opts.seed >= 0 ? "yes" : "no"));
  pythia.readString("Random:seed = "+std::to_string(opts.seed));

  // Beam parameter settings. Values below agree with default ones.
  pythia.readString("Beams:idA                    = 2212");
  pythia.readString("Beams:idB                    = 2212");
  pythia.readString("Beams:eA                     = 7000.");
  pythia.readString("Beams:eB                     = 7000.");
  pythia.readString("Beams:eCM                    = 14000.");//As above
  pythia.readString("SoftQCD:inelastic            = on");
  pythia.readString("ParticleDecays:limitTau0     = on");
  pythia.readString("ParticleDecays:tau0Max       = 10");
  pythia.readString("Tune:ee                      = 7");
  pythia.readString("Tune:pp                      = 14");
  
  // Initialize
  pythia.init();

  // Output 
  std::ofstream* fout = nullptr;
  if (not opts.outputFileName.empty() and opts.outputFileName != "-") 
    fout = new std::ofstream(opts.outputFileName.c_str());
  std::ostream& out = fout ? *fout : std::cout;
  
  // HepMC interface
  HepMC3::GenEvent        event;
  HepMC3::WriterAscii     writer(out);
  HepMC3::Pythia8ToHepMC3 converter;


  size_t nerr     = 0;
  size_t maxError = 10;
  for (size_t iev = 0; iev < opts.nev; ++iev) {
    if (not pythia.next()) {
      if (++nerr >= maxError) {
        std::cerr << "Event generation failed " << nerr << " times"
                  << std::endl;
        return 1;
      }
    }

    event.clear();
#ifdef OLD_INTERFACE
    converter.fill_next_event(pythia, &event);
#else 
    converter.fill_next_event(pythia, event);
#endif
    writer.write_event(event);
  }

  return 0;
}
// ===================================================================
//
// EOF
//

    
  
