//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef LOGGER_HH
#define LOGGER_HH
#include <iostream>
#include <fstream>
#include <iomanip>
#include <filesystem>
#ifdef __VERSION__ // Compiler version string
# ifdef __GNUC__
#  define COMPILER_VERSION "GCC " __VERSION__ 
# else
#  define COMPILER_VERSION __VERSION__
# endif
#else
# define COMPILER_VERSION "?"
#endif

namespace bench {
  //__________________________________________________________________
  /** Log execution.  Meant to be used with timers.  This will output
      the test and version of HepMC3 used for the test as the two
      first lines */ 
  struct logger
  {
    logger(const std::string& fileName,
           const std::string& inputFileName,
           const std::string& test,
           const std::string& version,
           std::ostream& stream=std::cout)
      : _stream(stream)
    {
      if (not fileName.empty() and fileName != "-")
        _file = new std::ofstream(fileName.c_str());

      std::filesystem::path p(inputFileName);
      this->stream() << p.stem() << "\n"
                     << test << "\n"
                     << version << "\n"
                     << COMPILER_VERSION << std::endl;
    }
    ~logger()
    {
      if (_file) {
        _file->close();
        delete _file;
      }
    }
    
    operator std::ostream& ()
    {
      if (_file) return *_file;
      return _stream;
    }
  
    std::ostream& stream() {
      if (_file) return *_file;
      return _stream;
    }
    
    std::ostream& _stream;
    std::ofstream* _file = nullptr;
  };
}

#endif
//
// EOF
//
