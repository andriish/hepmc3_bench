//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCHMARK_OPTIONS_HH
#define BENCHMARK_OPTIONS_HH
#include <string>
#include <iostream>

namespace bench {
  //__________________________________________________________________
  /** Structure holding options
   */
  struct options { 
    std::string inputFileName  = "-";
    std::string outputFileName = "-";
    std::string logFileName    = "-";
    int         maxEvents      = -1;
    int         firstEvent     = 0;
    bool        timing         = false;
    int         verbose        = 0;
  };

  //__________________________________________________________________
  /** Display program usage help

      @param progname  Program name
      @param opts      Options
      @param out       Output stream
  */
  void usage(const std::string& progname,
             options& opts,
             std::ostream& out=std::cout)
  {
    out << "Usage: " << progname << " [OPTIONS] [INPUT]\n\n"
        << "Options:\n"
        << "  -h,--help Show this help\n"
        << "  -i,--input       INPUT  File to read, flag optional ("
        << opts.inputFileName << ")\n"
        << "  -o,--output      OUTPUT File to write ("
        << opts.outputFileName << ")\n"
        << "  -l,--log         LOG    File to write to ("
        << opts.logFileName << ")\n"
        << "  -n,--max-events  NEV    Maximum number of events to read ("
        << opts.maxEvents << ")\n"
        << "  -f,--first-event IEV    First event to process ("
        << opts.maxEvents << ")\n"
        << "  -v,--verbose            Increase verbosity level ("
        << opts.verbose << ")\n"
        << "  -t,--time               Enable timing output\n\n"
        << "If INPUT is \"-\", then read from standard input\n"
        << "If LOG is \"-\", then write to standard output\n"
        << "OUTPUT only makes sense for select tests\n"
        << "The option -v,--verbose can be given multiple times\n"
        << std::endl;
  }
  
  
  //__________________________________________________________________
  /** Parse command line options, and store result in passed options
      structure.

      @param argc   number of command line options
      @param argv   Command line arguments
      @param opt    Options structure
      
      @return negative number on error, 0 program should exit,
      positive if all good */ 
  int parse(int argc, char** argv, options& opt) {
    for (int i = 1; i < argc; ++i) {
      if (argv[i][0] == '-') {
        if (argv[i][1] == '-') {
          std::string arg(argv[i]);
          
          if (arg == "--help") {
            usage(argv[0], opt);
            return 0;
          }
          else if (arg == "--input")
            opt.inputFileName = argv[++i];
          else if (arg == "--output")
            opt.outputFileName = argv[++i];
          else if (arg == "--log")
            opt.logFileName = argv[++i];
          else if (arg == "--max-events")
            opt.maxEvents = std::stoi(argv[++i]);
          else if (arg == "--first-event")
            opt.firstEvent = std::stoi(argv[++i]);
          else if (arg == "--verbose")
            opt.verbose++;
          else if (arg == "--time")
            opt.timing = true;
          else {
            std::cerr << "Unknown option: " << argv[i] << std::endl;
            return 1;
          }
          
          continue;
        }
        switch (argv[i][1]) {
        case 'h': usage(argv[0],opt);                       return 0;
        case 'i': opt.inputFileName = argv[++i];            break;
        case 'o': opt.outputFileName = argv[++i];           break;
        case 'l': opt.logFileName   = argv[++i];            break;
        case 'n': opt.maxEvents     = std::stoi(argv[++i]); break;
        case 'f': opt.firstEvent    = std::stoi(argv[++i]); break;
        case 'v': opt.verbose++;                            break;
        case 't': opt.timing        = true;                 break;
      default:
        std::cerr << "Unknwon opton: " << argv[i] << std::endl;
        return 1;
      }
    }
    else
      opt.inputFileName = argv[i];
    }
    return 1;
  }
}
#endif
//
// EOF
//
