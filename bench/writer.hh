//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCH_WRITER_HH
#define BENCH_WRITER_HH
#include <HepMC3/Writer.h>
#include <HepMC3/WriterAscii.h>
#include <HepMC3/WriterAsciiHepMC2.h>
#include <HepMC3/WriterHEPEVT.h>
// #include <HepMC3/CompressedIO.h>
// #include <HepMC3/WriterGZ.h>
#include <filesystem>
namespace bench
{
  //__________________________________________________________________
  /** Pointer to writer */
  using writerptr = std::shared_ptr<HepMC3::Writer>;
  //__________________________________________________________________
  /** Create a writer.  If the output file name is empty or "-", then
      setup ASCII writer to standard output.

      @param outputFileName  File to read from.

      @return writer of events
  */
  writerptr writer(const std::string& outputFileName)
  {
    using Ascii=HepMC3::WriterAscii;
    using Ascii2=HepMC3::WriterAsciiHepMC2;
    using HEPEVT=HepMC3::WriterHEPEVT;
    // template <typename T, HepMC3::Compression C=HepMC3::Compression:z>
    // using GZ=HepMC3::WriterGZ<T,C>;
    
    if (outputFileName.empty() or outputFileName == "-")
      return std::make_shared<Ascii>(std::cout);

    std::filesystem::path p(outputFileName);
    auto ext = p.extension();

    
    if (ext == ".hepmc")
      return std::make_shared<Ascii>(outputFileName);
    // if (ext == ".hepmcgz")
    //   return std::make_shared<GZ<Ascii>>(outputFileName);
    if (ext == ".hepmc2")
      return std::make_shared<Ascii2>(outputFileName);
    // if (ext == ".hepmc2gz")
    //   return std::make_shared<GZ<Ascii2>>(outputFileName);
    if (ext == ".hepevt")
      return std::make_shared<HEPEVT>(outputFileName);
    // if (ext == ".hepevtgz")
    //   return std::make_shared<GZ<HEPEVT>>(outputFileName);

    std::cout << "Cannot deduce writer from filename \""
              << outputFileName << " (" << ext << ")" << std::endl;

    return nullptr;
  }
}
#endif
