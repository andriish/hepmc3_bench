//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCH_VERSION_HH
#define BENCH_VERSION_HH
#include <string>
#include <filesystem>

//__________________________________________________________________
std::string version(const std::string& progname,
                    const std::string& test)
{
  std::filesystem::path prog(progname);
  std::string           stem = prog.stem();

  return stem.substr(test.length()+1);
}

//__________________________________________________________________
template <typename Func>
std::string testName(Func)
{
  return __PRETTY_FUNCTION__;
}
#endif
