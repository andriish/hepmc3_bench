//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCHMARK_READ_HH
#define BENCHMARK_READ_HH
#include <HepMC3/Reader.h>
#include <HepMC3/ReaderFactory.h>
#include <HepMC3/GenEvent.h>
#include <bench/options.hh>

namespace bench {
  //__________________________________________________________________
  /** Smart pointer to reader */
  using readerptr = std::shared_ptr<HepMC3::Reader>;
  
  //__________________________________________________________________
  /** Read in one event

      @param reader  Reader of events
      @param event   Structure to read event into

      @return true on success
   */
  bool read(readerptr& reader, HepMC3::GenEvent& event)
  {
    return reader->read_event(event) and not reader->failed();
  }

  //__________________________________________________________________
  void show(HepMC3::GenEvent& event, options& opt)
  {
    if (opt.verbose > 1)
      std::cerr << "Event # "
                << std::setw(4) << event.event_number()
                << " with "
                << std::setw(6) << event.particles().size()
                << " particles and "
                << std::setw(5) << event.vertices().size()
                << " vertices" << std::endl;
  }
  //__________________________________________________________________
  /** Test if we have more events to read

      @param iev Current event number
      @param max Maximum number of events, positive

      @return true if we're ok for reading more events 
   */
  bool more(int iev, int max)
  {
    return max <= 0 or iev < max;
  }

  //__________________________________________________________________
  /** Create a reader.  If the input file name is empty or "-", then
      setup ASCII reader from standard input.

      @param inputFileName  File to read from.

      @return reader of events
  */
  readerptr reader(const std::string& inputFileName)
  {
    if (inputFileName == "-")
      return std::make_shared<HepMC3::ReaderAscii>(std::cin);

    return HepMC3::deduce_reader(inputFileName);
  }
  
}

#endif
//
// EOF
//

