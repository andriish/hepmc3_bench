//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCHMARK_TIMER_HH
#define BENCHMARK_TIMER_HH
#include <chrono>
#include <iostream>

namespace bench {
  //__________________________________________________________________
  /** A timer, works as a guard */
  struct timer
  {
    /** The clock to use */
    using clock = std::chrono::high_resolution_clock;
    using point = typename clock::time_point;
    using nano  = std::chrono::nanoseconds;

    /** Constructor
      
        @param out where to write result
    */
    timer(std::ostream& out, size_t n, bool enable)
      : _out(out), _n(n), _enable(enable)
    {}

    /** Destructor, writes out result */
    ~timer()
    {
      if (not _enable) return;
      if (_n <= 0) return;
    
      auto end = clock::now();
      auto ns  = std::chrono::duration_cast<nano>(end - _start);

      _out << _n << "\t" << ns.count() << std::endl;
    }

    /** Start of timer */
    point _start = clock::now();
    /** output stream */
    std::ostream& _out;
    /** Size of event */
    size_t _n;
    /** Enable */
    bool _enable;
  };
}
#endif
//
// EOF
//
