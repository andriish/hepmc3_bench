//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#ifndef BENCH_DRIVER_HH
#define BENCH_DRIVER_HH
#include <HepMC3/Setup.h>
#include <bench/reader.hh>
#include <bench/logger.hh>
#include <bench/version.hh>
#include <bench/options.hh>
#include <bench/timer.hh>
/** A macro to deduce the name of test from a function name */
#define NAME(func) (void(&func),#func)

namespace bench {
  //__________________________________________________________________
  /** Loop over data and call test on each event.

      Note, one can in principle specialise this on different function
      to get different behaviours for particular functions.  For
      example, in the `read` test, this template is specialised on
      `nullptr` to not do anything with the events, but time the read.

      @param reader   Reader of data
      @param opts     Options
      @param log      Log stream
      @param test     Test function to execute on events
   */
  template <typename Test>
  void looper(readerptr         reader,
              options&          opts,
              logger&           log,
              Test              test)
  {
    HepMC3::GenEvent event;
    int iev = 0;
    while (more(iev++, opts.maxEvents) && read(reader, event)) {
      if (iev < opts.firstEvent) continue;
      show(event, opts);

      timer t(log, event.particles().size(), opts.timing);
      test(event);
    }
  }
  
  //__________________________________________________________________
  /** Driver code

      @param name   Name of the test
      @param test   Test function to execute on events
      @param argc   Number of command line arguments
      @param argv   Command line arguments

      @return Program return code (0 is success)
   */
  template <typename Test>
  int driver(const std::string& name, Test test, int argc, char** argv)
  {
    // Decuce version from program name
    std::string  vers = version(argv[0], name);

    // Default value of options, and parse command line 
    options      opts;
    int          ret = parse(argc, argv, opts);
    if (ret <= 0) return -ret;

    // Make HepMC3 be very verbose 
    if (opts.verbose > 2) HepMC3::Setup::set_debug_level(30);
    
    // Set-up logger 
    logger  log(opts.logFileName, opts.inputFileName, name, vers);

    // Set-up reader 
    readerptr reader = bench::reader(opts.inputFileName);
    if (not reader) {
      std::cerr << "Failed to make reader for "
                << opts.inputFileName << std::endl;
      return 1;
    }

    // Loop over the data.  This uses the template function defined
    // above.  Note, that if the passed test is `nullptr` then the
    // second specialisation if that template is used.
    looper(reader, opts, log, test);

    // Close the reader 
    reader->close();

    return 0;
  }
}

#endif
//
// EOF
//

