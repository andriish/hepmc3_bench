//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <HepMC3/FourVector.h>

void rotate(HepMC3::GenEvent& event)
{
  event.rotate(HepMC3::FourVector(0.2,0.3,0.4,0.5));
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver(NAME(rotate),rotate,argc,argv);
}
//
// EOF
//


      
        
        
        
