//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>

namespace bench {
  //__________________________________________________________________
  /** Read in one event, and output time it took to read 

      @param reader  Reader of events
      @param event   Structure to read event into
      @param log     Log output
      @param enable  Enable timer 

      @return true on success
   */
  bool timed_read(readerptr& reader,
                  HepMC3::GenEvent& event,
                  std::ostream& log,
                  bool enable=true)
  {
    timer t(log, 0, enable);

    bool ret = read(reader, event);

    t._n = event.particles().size();
    
    return ret;
  }
  
  //------------------------------------------------------------------
  /** Loop over events and possibly time the reading. This does not
      execute any function on the event itself.

      @param reader   Reader of data
      @param opts     Options
      @param logger   Log stream
   */
  template <>
  void looper<std::nullptr_t>(readerptr         reader,
                              options&          opts,
                              logger&           log,
                              std::nullptr_t    )
  {
    HepMC3::GenEvent event;
    int iev = 0;
    while (more(iev++, opts.maxEvents) and
           timed_read(reader, event, log, opts.timing)) {
      show(event, opts);
    }
  
  }
}

//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver("read",nullptr,argc,argv);
}
//
// EOF
//


      
        
        
        
