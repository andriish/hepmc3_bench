//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>

//--------------------------------------------------------------------
std::shared_ptr<HepMC3::IntAttribute> a(int val)
{
  return std::make_shared<HepMC3::IntAttribute>(val);
}

//--------------------------------------------------------------------
void add_attr(HepMC3::GenEvent& event)
{
  for (auto particle : event.particles())
    particle->add_attribute("some", a(particle->id()+10));
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver("add_attr",add_attr,argc,argv);
}
//
// EOF
//


      
        
        
        
