//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>

void new_delete(HepMC3::GenEvent& event)
{
  auto copy = new HepMC3::GenEvent(event);
  delete copy;
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver(NAME(new_delete),new_delete,argc,argv);
}
//
// EOF
//


      
        
        
        
