//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <HepMC3/Print.h>

void print(HepMC3::GenEvent& event)
{
  std::ofstream out("/dev/null");
  HepMC3::Print::listing(out, event);
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  
  return bench::driver(NAME(print),print,argc,argv);
}
//
// EOF
//


      
        
        
        
