//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <bench/writer.hh>
#include <HepMC3/WriterAscii.h>

namespace bench {
  //------------------------------------------------------------------
  /** Loop over events and possibly time the reading. This is
      specialised so that we may declare the writer outside of the
      event loop.

      @param reader   Reader of data
      @param opts     Options
      @param logger   Log stream
   */
  template <>
  void looper<std::nullptr_t>(readerptr         reader,
                              options&          opts,
                              logger&           log,
                              std::nullptr_t    )
  {
    writerptr writer = bench::writer(opts.outputFileName);

    HepMC3::GenEvent event;
    int iev = 0;
    while (more(iev++, opts.maxEvents) and read(reader, event)) {
      if (iev < opts.firstEvent) continue;
      show(event, opts);

      timer t(log, event.particles().size(), opts.timing);
      writer->write_event(event);
    }
  
  }
}
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{
  return bench::driver("write",nullptr,argc,argv);
}
//
// EOF
//


      
        
        
        
