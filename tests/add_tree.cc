//
// Copyright (c) 2023 Christian Holm Christensen <cholmcc@gmail.com>
// Distributed under the GPL-3
//
#include <bench/driver.hh>
#include <list>

HepMC3::GenParticlePtr particle(int pid,
                                double px,
                                double py,
                                double pz,
                                double e,
                                int    status,
                                HepMC3::GenVertexPtr prod=nullptr)
{
  auto p =
    std::make_shared<HepMC3::GenParticle>(HepMC3::FourVector(px,py,pz,e),
                                          pid, status);

  if (prod) prod->add_particle_out(p);

  return p;
}

template <typename ...Args>
HepMC3::GenVertexPtr vertex(double x,
                            double y,
                            double z,
                            double t,
                            Args... args)
{
  auto v =
    std::make_shared<HepMC3::GenVertex>(HepMC3::FourVector(x,y,z,t));
  
  for (auto p : {args...}) v->add_particle_in(p);

  return v;
}
    

size_t sub(HepMC3::GenParticlePtr& p,
           std::list<HepMC3::GenParticlePtr>& leaves) {
  p->set_status(2);
  auto v  = vertex(0,0,0,0, p);
  auto p1 = particle(0,   0,     7000,  7000,2212,1,v);
  auto p2 = particle(0.75,-1.569,32.191,32.238,1, 1,v);
  leaves.emplace_back(p1);
  leaves.emplace_back(p2);

  return 2;
}

void add_tree(HepMC3::GenEvent& event)
{
  size_t n = event.particles().size();
  event.clear();

  auto b1 = particle(0,0,7000,7000,2212,4);
  auto b2 = particle(0.75,-1.569,32.191,32.238,1,4);
  auto v1 = vertex(0,0,0,0,b1,b2);
  auto p1 = particle(0.75,-1.569,32.191,-32.238,1,3);

  std::vector<HepMC3::GenParticlePtr> beams{b1,b2};
  std::list<HepMC3::GenParticlePtr> leaves{p1};
  
  size_t i    = 3;
  bool   more = true;
  while (more) {
    std::list<HepMC3::GenParticlePtr> newLeaves;
    for (auto p : leaves) {
      i += sub(p, newLeaves);
      if (i >= n) {
        more = false;
        break;
      }
    }
    if (not more) break;
    leaves = newLeaves;
  }
  
  event.add_tree(beams);
}
  
//--------------------------------------------------------------------
int
main(int argc, char** argv)
{  
  return bench::driver(NAME(add_tree),add_tree,argc,argv);
}
//
// EOF
//


      
        
        
        
